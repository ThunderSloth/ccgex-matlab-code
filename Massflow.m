function val = Massflow (p1,p2,T1,Di,Do,mu1)
rho1 = p1./(287.*T1); 
dp = p1-p2;
eps1 = 1-(0.41+0.35.*(Di/Do)^4).*dp./(1.4.*p1);
b=double(p2./p1>=0.75);
eps1=eps1.*b+(1-b);
Re0 = 1e6;
Re_D=zeros(length(dp),1);
    for i=1:length(dp)
    Re_D(i) = fzero(@(x)DeltaMF(x,dp(i),rho1(i),mu1(i),eps1(i),Di,Do),Re0);
    end
val = Re_D.*pi./4.*Do.*mu1;
val=val(isnan(val)==false);
end

function  dMF = DeltaMF (Re_D,dp,rho1,mu1,eps1,Di,Do)
beta = Di./Do;
qm_Re_D = Re_D.*pi./4.*Do.*mu1;
C = ReaderHarrisGallagher(Re_D, Do, beta);
qm_or = C.*eps1.*pi./4.*Di.^2.*sqrt(2.*dp.*rho1./(1-beta.^4));
dMF = qm_or-qm_Re_D;
end

function [ C ] = ReaderHarrisGallagher (Re_D, Do, beta)
%READERHARRISGALLAGHER Reader-Harris/Gallagher equation
    A_c = (19000.*beta./Re_D).^0.8;
    C = 0.5961 + 0.0261.*beta^2 - 0.216.*beta.^8 + ...
        0.000521.*(beta.*1e6./Re_D).^0.7 + ...
        (0.0188+0.0063.*A_c).*beta.^3.5 * (1e6./Re_D).^0.3;
    C(Do < 0.0712)=C(Do < 0.0712) + 0.011.*(0.75-beta).*(2.8-Do.*1e3./25.4);

end
