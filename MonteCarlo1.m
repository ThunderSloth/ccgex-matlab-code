% Run Monte-Carlo simulation of orfice uncertainty
numIt = 5e3;    % Number of iterations

% Orifice parameters; leave these unchanged
Do = 0.05; % m
Di = 0.03; % m
p1_mean = 125e3;        % Pa
p2_mean = 110e3;        % Pa
T1_mean = 90+273.15;    % K
% Uncertainties
up1 = 100;
up2 = 100;
uT1 = 0.3;
% end Orifice parameters

tic

p1r = p1_mean+up1*randn(numIt,1);
p2r= p2_mean+up2*randn(numIt,1);
T1r= T1_mean+uT1*randn(numIt,1);
mu1= 145.8.*T1r.^1.5./(T1r+110.4).*1e-8; % Sutherland's law
MF = Massflow(p1r,p2r,T1r,Di,Do,mu1);

toc

histogram (MF);
mean (MF)
std (MF)

